package com.harry.performance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileReaderWriter {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader("textFile"));
		BufferedWriter bw = new BufferedWriter(new FileWriter("textFileCopy"));

		String s;

		while ((s = br.readLine()) != null) {

			System.out.println(s);
			bw.write(s);
			bw.newLine();

		}
		br.close();
		bw.close();
	}


}
