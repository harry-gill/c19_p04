package com.harry.performance;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class UnbufferedFileIO {

	/*
	 * This program writes a large text to a file and reads it back.
	 * Read and Write is done using low level classes FileInputStream and FileOutputStream
	 * which are unbuffered.
	 * The read and write operations are timed and printed.
	 * */
	public static void main(String[] args) throws IOException {

		System.out.println("Program started....");
		StringBuilder sb = new StringBuilder();
		// 5 mb data
		for (int i = 0; i < 5_000_000; i++) {
			sb.append("a");
		}
		String s = sb.toString();

		long startTime = System.currentTimeMillis();
		try (var out = new FileOutputStream("myFile1")) {
			for (byte b : s.getBytes()) {
				out.write(b);
			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("File is written in " + (endTime - startTime) + " ms");

		startTime = System.currentTimeMillis();
		try (var in = new FileInputStream("myFile1")) {
			while (in.read() != -1) {
				// Reading data but not printing/using
			}
		}
		endTime = System.currentTimeMillis();
		System.out.println("File is read in " + (endTime - startTime) + " ms");
	}
}
